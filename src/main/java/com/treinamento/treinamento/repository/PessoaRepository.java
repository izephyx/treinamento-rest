package com.treinamento.treinamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.treinamento.treinamento.entity.Pessoa;


@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
	Pessoa findById(long pId);
}
