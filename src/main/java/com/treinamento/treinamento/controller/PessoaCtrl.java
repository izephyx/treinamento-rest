package com.treinamento.treinamento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.treinamento.treinamento.entity.Pessoa;
import com.treinamento.treinamento.repository.PessoaRepository;

@RestController
@RequestMapping(path ="/api")
public class PessoaCtrl {
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	
	public void setPessoaRepository(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}


	@GetMapping("/pessoas")
	public Iterable<Pessoa> get(){
		return pessoaRepository.findAll();
	}
	
	@GetMapping("/pessoas/obterPorId")
	public Pessoa getId(
		@RequestParam(name = "pId", required = true) long pId) {
		
		Pessoa lPessoa = null;
		lPessoa = pessoaRepository.findById(pId);
		
		
		return lPessoa;
	}
	
	
	@PostMapping("/pessoas")
	public Pessoa post(
		@RequestParam(name = "pNome", required = true) String pNome) {
		
		Pessoa lPessoa = new Pessoa();
		lPessoa.setNome(pNome);
		pessoaRepository.save(lPessoa);
		
		return lPessoa;
	}
	
	@DeleteMapping("/pessoas{pId}")
	public Pessoa delete(
		@RequestParam(name = "pId", required = true) long pId) {
		
		Pessoa lPessoa = null;
		lPessoa = pessoaRepository.findById(pId);
		pessoaRepository.delete(lPessoa);
		
		return lPessoa;
	}
	
	@PutMapping("/pessoas{pId, pNome}")
	public Pessoa put(
		@RequestParam(name = "pId", required = true) long pId,
		@RequestParam(name = "pNome", required = true) String pNome) {
		
		Pessoa lPessoa = null;
		lPessoa = pessoaRepository.findById(pId);
		lPessoa.setNome(pNome);
		pessoaRepository.save(lPessoa);
		return lPessoa;
	}
}
